core = "7.x"
api = "2"

; Drupal

projects[drupal][type] = core
projects[drupal][version] = 7

; Contrib projects

projects[admin_menu][subdir] = "contrib"
projects[admin_menu][version] = "3"

projects[context][subdir] = "contrib"
projects[context][version] = "3"

projects[ctools][subdir] = "contrib"
projects[ctools][version] = "1"

;projects[cuddlecake][type] = profile
;projects[cuddlecake][download][type] = git
;projects[cuddlecake][download][url] = "https://betz@bitbucket.org/betz/drupal-build.git"

projects[date][subdir] = "contrib"
projects[date][version] = "2"

projects[devel][subdir] = "contrib"
projects[devel][version] = "1"

projects[diff][subdir] = "contrib"
projects[diff][version] = "2"

projects[features][subdir] = "contrib"
projects[features][version] = "1"

projects[filefield_sources][subdir] = "contrib"
projects[filefield_sources][version] = "1"

projects[google_analytics][subdir] = "contrib"
projects[google_analytics][version] = "1"

projects[image_resize_filter][subdir] = "contrib"
projects[image_resize_filter][version] = "1"

projects[insert][subdir] = "contrib"
projects[insert][version] = "1"

projects[linkit][subdir] = "contrib"
projects[linkit][version] = "1"

projects[mollom][subdir] = "contrib"
projects[mollom][version] = "1"

projects[pathauto][subdir] = "contrib"
projects[pathauto][version] = "1"

projects[pathologic][subdir] = "contrib"
projects[pathologic][version] = "1"

projects[redirect][subdir] = "contrib"
projects[redirect][version] = "1"

projects[smtp][subdir] = "contrib"
projects[smtp][version] = "1"

projects[spamspan][subdir] = "contrib"
projects[spamspan][version] = "1"

projects[strongarm][subdir] = "contrib"
projects[strongarm][version] = "2"

projects[token][subdir] = "contrib"
projects[token][version] = "1"

projects[transliteration][subdir] = "contrib"
projects[transliteration][version] = "3"

projects[video_embed_field][subdir] = "contrib"
projects[video_embed_field][version] = "2"

projects[views][subdir] = "contrib"
projects[views][version] = "3"

projects[wysiwyg][subdir] = "contrib"
projects[wysiwyg][version] = "2"

projects[wysiwyg_filter][subdir] = "contrib"
projects[wysiwyg_filter][version] = "1"

libraries[tinymce][download][type] = "get"	
libraries[tinymce][download][url] = "https://github.com/downloads/tinymce/tinymce/tinymce_3.4.8.zip"
libraries[tinymce][destination] = "libraries"



